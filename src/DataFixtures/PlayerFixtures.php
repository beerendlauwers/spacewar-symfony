<?php

namespace App\DataFixtures;

use App\Entity\Player;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PlayerFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $playerOne = new Player();
        $playerOne
            ->setName("beerend")
            ->setPassword($this->passwordEncoder->encodePassword($playerOne, "beerend"))
            ->setMetal(100)
            ->setCrystal(100)
            ->setEnergy(100);
        $manager->persist($playerOne);
        $this->addReference('player-one', $playerOne);

        $playerTwo = new Player();
        $playerTwo
            ->setName("other")
            ->setPassword($this->passwordEncoder->encodePassword($playerTwo, "other"))
            ->setMetal(100)
            ->setCrystal(100)
            ->setEnergy(100);
        $manager->persist($playerTwo);
        $this->addReference('player-two', $playerTwo);

        $manager->flush();
    }
}
