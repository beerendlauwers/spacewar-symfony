<?php

namespace App\DataFixtures;

use App\Entity\Ship;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ShipFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $scout = $this->getReference('scout');
        $frigate = $this->getReference('frigate');
        $bomber = $this->getReference('bomber');

        $playerOne = $this->getReference('player-one');
        $playerTwo = $this->getReference('player-two');

        foreach ([$scout, $frigate, $bomber] as $shipType) {
            $ship = new Ship();
            $ship->setShipType($shipType)->setAmount(5)->setPlayer($playerOne);

            $shipOther = new Ship();
            $shipOther->setShipType($shipType)->setAmount(3)->setPlayer($playerTwo);

            $manager->persist($ship);
            $manager->persist($shipOther);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [PlayerFixtures::class, ShipTypeFixtures::class];
    }
}
