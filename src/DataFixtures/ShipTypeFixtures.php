<?php

namespace App\DataFixtures;

use App\Entity\ShipType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class ShipTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $shipTypes = [
            [
                'name' => 'Scout',
                'metal' => 50,
                'crystal' => 20,
                'energy' => 30,
                'health' => 100,
                'strength' => 5,
                'image' => 'scout.gif',
                'ref' => 'scout',
            ],
            [
                'name' => 'Frigate',
                'metal' => 200,
                'crystal' => 100,
                'energy' => 120,
                'health' => 250,
                'strength' => 30,
                'image' => 'frigate.gif',
                'ref' => 'frigate',
            ],
            [
                'name' => 'Bomber',
                'metal' => 500,
                'crystal' => 800,
                'energy' => 200,
                'health' => 500,
                'strength' => 80,
                'image' => 'bomber.gif',
                'ref' => 'bomber',
            ],
        ];

        foreach ($shipTypes as $shipTypeData) {
            $shipType = new ShipType();
            $shipType
                ->setName($shipTypeData['name'])
                ->setHealth($shipTypeData['health'])
                ->setImage($shipTypeData['image'])
                ->setStrength($shipTypeData['strength'])
                ->setMetal($shipTypeData['metal'])
                ->setCrystal($shipTypeData['crystal'])
                ->setEnergy($shipTypeData['energy']);
            $manager->persist($shipType);

            $this->addReference($shipTypeData['ref'], $shipType);
        }

        $manager->flush();
    }
}
