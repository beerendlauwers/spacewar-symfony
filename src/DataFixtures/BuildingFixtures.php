<?php

namespace App\DataFixtures;

use App\Entity\Building;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BuildingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $metalMine = $this->getReference('metal-mine');
        $crystalMine = $this->getReference('crystal-mine');
        $solarCollector = $this->getReference('solar-collector');

        $playerOne = $this->getReference('player-one');
        $playerTwo = $this->getReference('player-two');

        foreach ([$metalMine, $crystalMine, $solarCollector] as $buildingType) {
            $building = new Building();
            $building->setBuildingType($buildingType)->setAmount(2)->setPlayer($playerOne);

            $buildingOther = new Building();
            $buildingOther->setBuildingType($buildingType)->setAmount(10)->setPlayer($playerTwo);

            $manager->persist($building);
            $manager->persist($buildingOther);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [PlayerFixtures::class, BuildingTypeFixtures::class];
    }
}
