<?php

namespace App\DataFixtures;

use App\Entity\BuildingType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BuildingTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $buildingTypes = [
            [
                'name' => 'Metal Mine',
                'type' => 1,
                'rate' => 15,
                'metal' => 50,
                'crystal' => 20,
                'energy' => 30,
                'health' => 100,
                'image' => 'metal_mine.jpg',
                'ref' => 'metal-mine',
            ],
            [
                'name' => 'Crystal Mine',
                'type' => 2,
                'rate' => 5,
                'metal' => 80,
                'crystal' => 10,
                'energy' => 60,
                'health' => 150,
                'image' => 'crystal_mine.jpg',
                'ref' => 'crystal-mine',
            ],
            [
                'name' => 'Solar Collector',
                'type' => 3,
                'rate' => 10,
                'metal' => 30,
                'crystal' => 50,
                'energy' => 0,
                'health' => 75,
                'image' => 'solar_collector.jpg',
                'ref' => 'solar-collector',
            ],
        ];

        foreach ($buildingTypes as $buildingTypeData) {
            $buildingType = new BuildingType();
            $buildingType
                ->setName($buildingTypeData['name'])
                ->setResourceType($buildingTypeData['type'])
                ->setGenerationRate($buildingTypeData['rate'])
                ->setImage($buildingTypeData['image'])
                ->setHealth($buildingTypeData['health'])
                ->setMetal($buildingTypeData['metal'])
                ->setCrystal($buildingTypeData['crystal'])
                ->setEnergy($buildingTypeData['energy']);
            $manager->persist($buildingType);

            $this->addReference($buildingTypeData['ref'], $buildingType);
        }

        $manager->flush();
    }
}
