<?php

namespace App\Form;

use App\Entity\Player;
use App\Repository\PlayerRepository;
use App\Validator\Constraints\CannotAttackYourself;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form that allows you to select a player to do battle with.
 */
class Attack extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var PlayerRepository $playerRepository */
        $playerRepository = $options['player_repository'];

        /** @var Player $currentPlayer */
        $currentPlayer = $options['current_player'];

        $players = $playerRepository->findAll();

        $builder->add('otherPlayer', ChoiceType::class, [
            'choices' => $players,
            'choice_label' => function ($player, $key, $value) {
                /** @var Player $player */
                return $player->getName();
            },
            'constraints' => [
                new CannotAttackYourself(['player' => $currentPlayer]),
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver
            ->setRequired('player_repository')
            ->setRequired('current_player');
    }
}