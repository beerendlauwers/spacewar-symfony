<?php

namespace App\Entity;

use App\Entity\Traits\HasResourcesTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BattleRepository")
 *
 * A battle between two players that records the time of the battle,
 * how many resources the attacker stole, and how much damage the attacker did.
 *
 */
class Battle
{
    use HasResourcesTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player", inversedBy="battles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $attacker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player")
     * @ORM\JoinColumn(nullable=false)
     */
    private $defender;

    /**
     * @Assert\Range(min=0, minMessage="Damage done cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $damageDone;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttacker(): ?Player
    {
        return $this->attacker;
    }

    public function setAttacker(?Player $attacker): self
    {
        $this->attacker = $attacker;

        return $this;
    }

    public function getDefender(): ?Player
    {
        return $this->defender;
    }

    public function setDefender(?Player $defender): self
    {
        $this->defender = $defender;

        return $this;
    }

    public function getDamageDone(): ?int
    {
        return $this->damageDone;
    }

    public function setDamageDone(int $damageDone): self
    {
        $this->damageDone = $damageDone;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Perform a battle between two players.
     *
     * @param Player $attacker The attacking player.
     * @param Player $defender The defending player.
     *
     * @throws \Exception Throws an exception if DateTime is given an invalid format.
     */
    public function doBattle(Player $attacker, Player $defender)
    {
        // Calculate how powerful the fleets are.
        $attackerStrength = 0;
        foreach ($attacker->getShips() as $ship) {
            $attackerStrength += $ship->getShipType()->getStrength() * $ship->getAmount();
        }

        $defenderStrength = 0;
        foreach ($defender->getShips() as $ship) {
            $defenderStrength += $ship->getShipType()->getStrength() * $ship->getAmount();
        }

        // Randomize the outcome somewhat.
        $attackerStrength *= random_int(7, 10) / 10;
        $defenderStrength *= random_int(8, 10) / 10;

        $attacks = [
            ['being_attacked' => $defender, 'strength_of_attack' => $attackerStrength],
            ['being_attacked' => $attacker, 'strength_of_attack' => $defenderStrength],

        ];

        // The function to calculate how many units are destroyed in an attack.
        $destroyFunction = function (int $unitHealth, int $unitAmount, int $enemyStrength): int {
            $numberOfUnitsDestroyed = floor($enemyStrength / $unitHealth);

            // We can't destroy units that don't exist, of course.
            $numberOfUnitsDestroyed = min($numberOfUnitsDestroyed, $unitAmount);

            return $numberOfUnitsDestroyed;
        };

        $attackerDamageDone = 0;
        foreach ($attacks as $attack) {
            $strength = $attack['strength_of_attack'];
            /** @var Player $beingAttacked */
            $beingAttacked = $attack['being_attacked'];

            // Destroy some of each other's ships.
            foreach ($beingAttacked->getShips() as $ship) {
                if ($strength <= 0) {
                    break;
                }

                $shipHealth = $ship->getShipType()->getHealth();
                $shipAmount = $ship->getAmount();

                $amountDestroyed = $destroyFunction($shipHealth, $shipAmount, $strength);

                // Reduce the strength by the amount that was expended on destroying the ships.
                $strength -= $shipHealth * $amountDestroyed;

                if ($beingAttacked === $defender) {
                    // Add the damage done to the tally.
                    $attackerDamageDone += $shipHealth * $amountDestroyed;
                }

                $ship->setAmount($shipAmount - $amountDestroyed);
            }

            // If there's still strength to go around, the attacker can attack the defender's buildings.
            if ($beingAttacked === $defender && $strength > 0) {
                foreach ($beingAttacked->getBuildings() as $building) {
                    if ($strength <= 0) {
                        break;
                    }

                    $buildingHealth = $building->getBuildingType()->getHealth();
                    $buildingAmount = $building->getAmount();

                    $amountDestroyed = $destroyFunction($buildingHealth, $buildingAmount, $strength);

                    // Reduce the strength by the amount that was expended on destroying the ships.
                    $strength -= ($buildingHealth * $amountDestroyed);

                    // Add the damage done to the tally.
                    $attackerDamageDone += $buildingHealth * $amountDestroyed;

                    $building->setAmount($buildingAmount - $amountDestroyed);
                }
            }
        }

        // The attacker can steal some resources from the defender.
        $percentageOfResourcesStolen = min(1.0, $attackerStrength / max($defenderStrength, 1.0));

        // The attacker always steals a few resources.
        $percentageOfResourcesStolen = max(0.05, $percentageOfResourcesStolen);

        // Reap the spoils!
        $metalStolen = $defender->getMetal() * $percentageOfResourcesStolen;
        if ($metalStolen >= 0) {
            $attacker->setMetal($attacker->getMetal() + $metalStolen);
            $defender->setMetal($defender->getMetal() - $metalStolen);
        }

        $crystalStolen = $defender->getCrystal() * $percentageOfResourcesStolen;
        if ($crystalStolen >= 0) {
            $attacker->setCrystal($attacker->getCrystal() + $crystalStolen);
            $defender->setCrystal($defender->getCrystal() - $crystalStolen);
        }

        $energyStolen = $defender->getEnergy() * $percentageOfResourcesStolen;
        if ($energyStolen >= 0) {
            $attacker->setEnergy($attacker->getEnergy() + $energyStolen);
            $defender->setEnergy($defender->getEnergy() - $energyStolen);
        }

        // Register the resources stolen in this battle.
        $this->setMetal($metalStolen);
        $this->setCrystal($crystalStolen);
        $this->setEnergy($energyStolen);
        $this->setAttacker($attacker);
        $this->setDefender($defender);
        $this->setDamageDone($attackerDamageDone);

        $now = new \DateTime("now");
        $this->setDate($now);
    }
}
