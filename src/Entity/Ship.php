<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShipRepository")
 *
 * A ship purchased by a player. Consists of a ship type
 * and the amount of ships the player has of that type.
 */
class Ship
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShipType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shipType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player", inversedBy="ships")
     * @ORM\JoinColumn(nullable=false)
     */
    private $player;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Amount cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShipType(): ?ShipType
    {
        return $this->shipType;
    }

    public function setShipType(?ShipType $shipType): self
    {
        $this->shipType = $shipType;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
