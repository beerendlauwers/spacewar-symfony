<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BuildingRepository")
 *
 * A building purchased by a player. Consists of a building type
 * and the amount of buildings the player has of that type.
 */
class Building
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BuildingType", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $buildingType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player", inversedBy="buildings", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $player;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Amount cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuildingType(): ?BuildingType
    {
        return $this->buildingType;
    }

    public function setBuildingType(?BuildingType $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
