<?php

namespace App\Entity;

use App\Entity\Traits\PurchasableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BuildingTypeRepository")
 *
 * A building type. Contains information about the building type, like price,
 * what it produces, and so on.
 */
class BuildingType
{
    use PurchasableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Resource type cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $resourceType;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Generation rate cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $generationRate;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Health cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $health;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getResourceType(): ?int
    {
        return $this->resourceType;
    }

    public function setResourceType(int $resourceType): self
    {
        $this->resourceType = $resourceType;

        return $this;
    }

    public function getGenerationRate(): ?int
    {
        return $this->generationRate;
    }

    public function setGenerationRate(int $generationRate): self
    {
        $this->generationRate = $generationRate;

        return $this;
    }

    public function getHealth(): ?int
    {
        return $this->health;
    }

    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
