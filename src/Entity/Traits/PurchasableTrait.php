<?php

namespace App\Entity\Traits;

use App\Entity\Player;

/**
 * Trait that makes an entity purchasable.
 * Relies on the entity having resources.
 */
trait PurchasableTrait
{
    use HasResourcesTrait;

    /**
     * Attempt to purchase the item by a player.
     *
     * @param Player   $player         The player that wishes to do the purchase.
     * @param \Closure $purchaseAction Function that does the item-specific actions
     *                                 required for the purchase.
     *
     * @return bool Whether the item was purchased or not.
     */
    public function attemptPurchaseBy(Player $player, \Closure $purchaseAction): bool
    {
        // Check if player can purchase this item.
        $hasEnoughMetal = $player->getMetal() >= $this->getMetal();
        $hasEnoughCrystal = $player->getCrystal() >= $this->getCrystal();
        $hasEnoughEnergy = $player->getEnergy() >= $this->getEnergy();

        if ($hasEnoughMetal && $hasEnoughCrystal && $hasEnoughEnergy) {
            $purchaseAction();
            $this->deductResourcesFrom($player);

            return true;
        }

        return false;
    }

    /**
     * Deduct resources from a player.
     *
     * @param Player $player Player to deduct resources from.
     */
    protected function deductResourcesFrom(Player $player)
    {
        $player->setMetal($player->getMetal() - $this->getMetal());
        $player->setCrystal($player->getCrystal() - $this->getCrystal());
        $player->setEnergy($player->getEnergy() - $this->getEnergy());
    }
}