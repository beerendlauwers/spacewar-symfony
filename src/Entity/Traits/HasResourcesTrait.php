<?php

namespace App\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait that adds the resources metal, crystal and energy to an entity.
 */
trait HasResourcesTrait
{
    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Resource cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $metal;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Resource cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $crystal;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=0, minMessage="Resource cannot be negative.")
     *
     * @ORM\Column(type="integer")
     */
    private $energy;

    public function getMetal(): int
    {
        return $this->metal;
    }

    public function setMetal(int $metal): self
    {
        $this->metal = $metal;

        return $this;
    }

    public function getCrystal(): int
    {
        return $this->crystal;
    }

    public function setCrystal(int $crystal): self
    {
        $this->crystal = $crystal;

        return $this;
    }

    public function getEnergy(): int
    {
        return $this->energy;
    }

    public function setEnergy(int $energy): self
    {
        $this->energy = $energy;

        return $this;
    }
}