<?php

namespace App\Entity;

use App\Entity\Traits\HasResourcesTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 * @UniqueEntity(fields={"name"}, message="There is already a player with that username.")
 * @ORM\HasLifecycleCallbacks()
 *
 * A player is able to purchase ships and buildings
 * and attack other players in battles.
 */
class Player implements UserInterface
{
    use HasResourcesTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLoginTime;

    /**
     * @var ArrayCollection|Building[] $buildings
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Building", mappedBy="player", orphanRemoval=true, cascade={"persist"})
     */
    private $buildings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ship", mappedBy="player", orphanRemoval=true)
     */
    private $ships;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Battle", mappedBy="attacker", orphanRemoval=true)
     */
    private $battles;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string")
     */
    private $password;

    public function __construct()
    {
        $this->buildings = new ArrayCollection();
        $this->ships = new ArrayCollection();
        $this->battles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastLoginTime(): ?\DateTimeInterface
    {
        return $this->lastLoginTime;
    }

    public function setLastLoginTime(?\DateTimeInterface $lastLoginTime): self
    {
        $this->lastLoginTime = $lastLoginTime;

        return $this;
    }

    /**
     * @return Collection|Building[]
     */
    public function getBuildings(): Collection
    {
        return $this->buildings;
    }

    /**
     * Get a Building by its building type.
     *
     * @param BuildingType $buildingType The building type we're looking for.
     *
     * @return Building|null The building, or null.
     */
    public function getBuildingByType(BuildingType $buildingType): ?Building
    {
        foreach ($this->buildings as $building) {
            if ($building->getBuildingType() === $buildingType) {
                return $building;
            }
        }

        return null;
    }

    /**
     * Add a building or update our current amount if we already have
     * buildings of this type.
     *
     * @param Building $building The building we want to add or update.
     *
     * @return Player
     */
    public function addOrUpdateBuilding(Building $building): self
    {
        $found = false;
        foreach ($this->buildings as $builtBuilding) {
            if ($builtBuilding->getBuildingType()->getId() === $building->getBuildingType()->getId()) {
                $builtBuilding->setAmount($builtBuilding->getAmount() + $building->getAmount());
                $found = true;
            }
        }

        if (!$found) {
            $this->buildings[] = $building;
            $building->setPlayer($this);
        }

        return $this;
    }

    /**
     * Remove a building from the player.
     *
     * @param Building $building The building we want to remove.
     *
     * @return Player
     */
    public function removeBuilding(Building $building): self
    {
        if ($this->buildings->contains($building)) {
            $this->buildings->removeElement($building);
            // set the owning side to null (unless already changed)
            if ($building->getPlayer() === $this) {
                $building->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * Purchase a new building.
     *
     * @param BuildingType $buildingType Type of building player wishes to purchase.
     * @param int          $amount       How many the player wishes to purchase.
     *
     * @return bool Whether the player has successfully purchased the building or not.
     */
    public function purchaseBuilding(BuildingType $buildingType, int $amount = 1): bool
    {
        $purchaseAction = function () use ($buildingType, $amount) {
            $building = new Building();
            $building->setBuildingType($buildingType)->setAmount($amount);
            $this->addOrUpdateBuilding($building);
        };

        return $buildingType->attemptPurchaseBy($this, $purchaseAction);
    }

    /**
     * @return Collection|Ship[]
     */
    public function getShips(): Collection
    {
        return $this->ships;
    }

    /**
     *
     * Get a Ship by its ship type.
     *
     * @param ShipType $shipType The ship type we're looking for.
     *
     * @return Ship|null The ship, or null.
     */
    public function getShipByType(ShipType $shipType): ?Ship
    {
        foreach ($this->ships as $ship) {
            if ($ship->getShipType() === $shipType) {
                return $ship;
            }
        }

        return null;
    }

    /**
     * Add a ship or update our current amount if we already have ships of this type.
     *
     * @param Ship $ship The ship we want to add or update.
     *
     * @return Player
     */
    public function addOrUpdateShip(Ship $ship): self
    {
        $found = false;
        foreach ($this->ships as $builtShip) {
            if ($builtShip->getShipType()->getId() === $ship->getShipType()->getId()) {
                $builtShip->setAmount($builtShip->getAmount() + $ship->getAmount());
                $found = true;
            }
        }

        if (!$found) {
            $this->ships[] = $ship;
            $ship->setPlayer($this);
        }

        return $this;
    }

    /**
     * Remove a ship from our fleet.
     *
     * @param Ship $ship The ship we want to remove.
     *
     * @return Player
     */
    public function removeShip(Ship $ship): self
    {
        if ($this->ships->contains($ship)) {
            $this->ships->removeElement($ship);
            // set the owning side to null (unless already changed)
            if ($ship->getPlayer() === $this) {
                $ship->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * Purchase a new ship.
     *
     * @param ShipType $shipType Type of ship player wishes to purchase.
     *
     * @return bool Whether the player has successfully purchased the ship or not.
     */
    public function purchaseShip(ShipType $shipType): bool
    {
        $purchaseAction = function () use ($shipType) {
            $ship = new Ship();
            $ship->setShipType($shipType)->setAmount(1);
            $this->addOrUpdateShip($ship);
        };

        return $shipType->attemptPurchaseBy($this, $purchaseAction);
    }

    /**
     * Calculate and update the player's resources.
     *
     * @param \DateTime $currentTime Current time.
     *
     * @return Player
     */
    public function calculateResources(\DateTime $currentTime): self
    {
        $lastLoginTime = $this->getLastLoginTime();

        if ($lastLoginTime && $currentTime > $lastLoginTime) {
            $diffSeconds = $currentTime->getTimestamp() - $lastLoginTime->getTimestamp();

            foreach ($this->getBuildings() as $building) {
                $amount = $building->getAmount();
                $buildingType = $building->getBuildingType();
                $generationRate = $buildingType->getGenerationRate();

                $amountGenerated = ceil($amount * $generationRate * ($diffSeconds / 60));

                switch ($buildingType->getResourceType()) {
                    case 1:
                        $this->setMetal($this->getMetal() + $amountGenerated);
                        break;
                    case 2:
                        $this->setCrystal($this->getCrystal() + $amountGenerated);
                        break;
                    case 3:
                        $this->setEnergy($this->getEnergy() + $amountGenerated);
                        break;
                    default:
                        throw new \InvalidArgumentException(sprintf('Invalid resource type %s.', $buildingType->getResourceType()));
                }

            }
        }

        $this->setLastLoginTime($currentTime);

        return $this;
    }


    /**
     * Calculate the resources of the player when they've been loaded from the database.
     *
     * @ORM\PostLoad
     *
     * @throws \Exception Throws an exception if DateTime is given an invalid format.
     */
    public function calculateResourcesOnLoad()
    {
        $now = new \DateTime("now", new \DateTimeZone('Europe/Amsterdam'));
        $this->calculateResources($now);
    }

    /**
     * @return Collection|Battle[]
     */
    public function getBattles(): Collection
    {
        return $this->battles;
    }

    /**
     * Add a battle to the player as the attacker.
     *
     * @param Battle $battle The battle we want to add.
     *
     * @return Player
     */
    public function addBattle(Battle $battle): self
    {
        if (!$this->battles->contains($battle)) {
            $this->battles[] = $battle;
            $battle->setAttacker($this);
        }

        return $this;
    }

    /**
     * Remove a battle from the player as the attacker.
     *
     * @param Battle $battle The battle we want to remove.
     *
     * @return Player
     */
    public function removeBattle(Battle $battle): self
    {
        if ($this->battles->contains($battle)) {
            $this->battles->removeElement($battle);
            // set the owning side to null (unless already changed)
            if ($battle->getAttacker() === $this) {
                $battle->setAttacker(null);
            }
        }

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->name;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "argon2i" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

}
