<?php

namespace App\Controller;

use App\Entity\Battle;
use App\Entity\Player;
use App\Form\Attack;
use App\Repository\BattleRepository;
use App\Repository\PlayerRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Controller for the game's login page and a player's base overview.
 */
class GameController extends AbstractController
{
    /**
     * @Route("/", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('game/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        // Logout functionality is handled by the security framework.
    }

    /**
     * @Route("/player/{name}", name="player_overview")
     *
     * @IsGranted("IS_PLAYER", subject="player")
     *
     */
    public function overview(Player $player, PlayerRepository $playerRepository, BattleRepository $battleRepository, ObjectManager $objectManager, Request $request)
    {
        $attackForm = $this->createForm(Attack::class, null, [
            'player_repository' => $playerRepository,
            'current_player' => $player,
        ]);

        $attackForm->handleRequest($request);
        if ($attackForm->isSubmitted() && $attackForm->isValid()) {
            /** @var Player $otherPlayer */
            $otherPlayer = $attackForm->getData()['otherPlayer'];

            $battle = new Battle();
            $battle->doBattle($player, $otherPlayer);

            $objectManager->persist($battle);
        }

        $objectManager->flush();

        $battles = $battleRepository->findAllBattlesContainingPlayer($player);

        return $this->render('game/overview.html.twig', [
            'player' => $player,
            'battles' => $battles,
            'attack_form' => $attackForm->createView(),
        ]);
    }
}