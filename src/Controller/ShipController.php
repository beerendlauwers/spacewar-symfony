<?php

namespace App\Controller;

use App\Entity\Player;
use App\Repository\PlayerRepository;
use App\Repository\ShipTypeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for purchasing a ship as a player.
 */
class ShipController extends AbstractController
{
    /**
     * @Route("/player/{name}/ship/add/{type}", name="add_ship")
     *
     * @IsGranted("IS_PLAYER", subject="player")
     */

    public function addShip(Player $player, $type, PlayerRepository $playerRepository, ShipTypeRepository $shipTypeRepository, ObjectManager $objectManager)
    {
        $shipType = $shipTypeRepository->find($type);

        if (!$shipType) {
            return new JsonResponse(['error' => 'Ship type not found.'], 400);
        }

        $purchased = $player->purchaseShip($shipType);
        $objectManager->flush();

        if (!$purchased) {
            return new JsonResponse(['error' => 'Insufficient funds.'], 400);
        }

        $response = [];
        if ($ship = $player->getShipByType($shipType)) {
            $response = ['amount' => $ship->getAmount(), 'type' => 'ship', 'id' => $shipType->getId()];
        }

        return new JsonResponse($response);
    }

}