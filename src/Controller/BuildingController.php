<?php

namespace App\Controller;

use App\Entity\Player;
use App\Repository\BuildingTypeRepository;
use App\Repository\PlayerRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for purchasing a building as a player.
 */
class BuildingController extends AbstractController
{
    /**
     * @Route("/player/{name}/building/add/{type}", name="add_building")
     *
     * @IsGranted("IS_PLAYER", subject="player")
     */
    public function addBuilding(Player $player, $type, PlayerRepository $playerRepository, BuildingTypeRepository $buildingTypeRepository, ObjectManager $objectManager)
    {
        $buildingType = $buildingTypeRepository->find($type);

        if (!$buildingType) {
            return new JsonResponse(['error' => 'Building type not found.'], 400);
        }

        $purchased = $player->purchaseBuilding($buildingType);
        $objectManager->flush();

        if (!$purchased) {
            return new JsonResponse(['error' => 'Insufficient funds.'], 400);
        }

        $response = [];
        if ($building = $player->getBuildingByType($buildingType)) {
            $response = ['amount' => $building->getAmount(), 'type' => 'building', 'id' => $buildingType->getId()];
        }

        return new JsonResponse($response);
    }
}