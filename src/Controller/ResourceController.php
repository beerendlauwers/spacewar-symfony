<?php

namespace App\Controller;

use App\Entity\Player;
use App\Repository\PlayerRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for checking the resources of a player.
 */
class ResourceController extends AbstractController
{
    /**
     * @Route("/player/{name}/resources", name="player_resources")
     *
     * @IsGranted("IS_PLAYER", subject="player")
     */
    public function getResources(Player $player, PlayerRepository $playerRepository)
    {
        return new JsonResponse([
            'metal' => $player->getMetal(),
            'crystal' => $player->getCrystal(),
            'energy' => $player->getEnergy(),
        ]);
    }
}