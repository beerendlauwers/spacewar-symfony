<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190331183110 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE building (id INT AUTO_INCREMENT NOT NULL, building_type_id INT NOT NULL, player_id INT NOT NULL, amount INT NOT NULL, INDEX IDX_E16F61D4F28401B9 (building_type_id), INDEX IDX_E16F61D499E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE building_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, resource_type INT NOT NULL, generation_rate INT NOT NULL, health INT NOT NULL, image VARCHAR(255) NOT NULL, metal INT NOT NULL, crystal INT NOT NULL, energy INT NOT NULL, UNIQUE INDEX UNIQ_2AFA207D5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, last_login_time DATETIME DEFAULT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, metal INT NOT NULL, crystal INT NOT NULL, energy INT NOT NULL, UNIQUE INDEX UNIQ_98197A655E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ship_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, health INT NOT NULL, strength INT NOT NULL, image VARCHAR(255) NOT NULL, metal INT NOT NULL, crystal INT NOT NULL, energy INT NOT NULL, UNIQUE INDEX UNIQ_2EFC84685E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ship (id INT AUTO_INCREMENT NOT NULL, ship_type_id INT NOT NULL, player_id INT NOT NULL, amount INT NOT NULL, INDEX IDX_FA30EB24CB61BCFE (ship_type_id), INDEX IDX_FA30EB2499E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE battle (id INT AUTO_INCREMENT NOT NULL, attacker_id INT NOT NULL, defender_id INT NOT NULL, damage_done INT NOT NULL, date DATETIME NOT NULL, metal INT NOT NULL, crystal INT NOT NULL, energy INT NOT NULL, INDEX IDX_1399173465F8CAE3 (attacker_id), INDEX IDX_139917344A3E3B6F (defender_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE building ADD CONSTRAINT FK_E16F61D4F28401B9 FOREIGN KEY (building_type_id) REFERENCES building_type (id)');
        $this->addSql('ALTER TABLE building ADD CONSTRAINT FK_E16F61D499E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE ship ADD CONSTRAINT FK_FA30EB24CB61BCFE FOREIGN KEY (ship_type_id) REFERENCES ship_type (id)');
        $this->addSql('ALTER TABLE ship ADD CONSTRAINT FK_FA30EB2499E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE battle ADD CONSTRAINT FK_1399173465F8CAE3 FOREIGN KEY (attacker_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE battle ADD CONSTRAINT FK_139917344A3E3B6F FOREIGN KEY (defender_id) REFERENCES player (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE building DROP FOREIGN KEY FK_E16F61D4F28401B9');
        $this->addSql('ALTER TABLE building DROP FOREIGN KEY FK_E16F61D499E6F5DF');
        $this->addSql('ALTER TABLE ship DROP FOREIGN KEY FK_FA30EB2499E6F5DF');
        $this->addSql('ALTER TABLE battle DROP FOREIGN KEY FK_1399173465F8CAE3');
        $this->addSql('ALTER TABLE battle DROP FOREIGN KEY FK_139917344A3E3B6F');
        $this->addSql('ALTER TABLE ship DROP FOREIGN KEY FK_FA30EB24CB61BCFE');
        $this->addSql('DROP TABLE building');
        $this->addSql('DROP TABLE building_type');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE ship_type');
        $this->addSql('DROP TABLE ship');
        $this->addSql('DROP TABLE battle');
    }
}
