<?php

namespace App\Repository;

use App\Entity\ShipType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShipType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShipType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShipType[]    findAll()
 * @method ShipType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShipTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShipType::class);
    }

    // /**
    //  * @return ShipType[] Returns an array of ShipType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShipType
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
