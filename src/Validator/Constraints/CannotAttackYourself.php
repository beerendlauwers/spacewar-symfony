<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Exception\MissingOptionsException;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * A constraint that checks that you're not trying to attack yourself.
 */
class CannotAttackYourself extends Constraint
{
    public $message = "You can't attack yourself.";

    public $player;

    public function __construct($options = null)
    {
        parent::__construct($options);

        if (null === $this->player) {
            throw new MissingOptionsException(sprintf('Provide another player for constraint %s', __CLASS__), ['player']);
        }
    }
}