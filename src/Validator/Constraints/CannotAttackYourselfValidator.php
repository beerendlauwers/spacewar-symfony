<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 *  A validator that checks that you're not trying to attack yourself.
 */
class CannotAttackYourselfValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CannotAttackYourself) {
            throw new UnexpectedTypeException($constraint, CannotAttackYourself::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if ($constraint->player === $value) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}