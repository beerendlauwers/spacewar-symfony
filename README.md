# Space War!

This is a Symfony project for an [Ogame](https://en.ogame.gameforge.com\/)-inspired browser game.
Players can purchase buildings to generate resources, purchase ships with said 
resources and then do battle with other players to plunder their resources.

## Requirements

- PHP >= 7.2
- MySQL >= 5.7 

## Setup

- Clone the project.
- `composer install`
- `yarn install`
- `./node_modules/.bin/encore dev`
- Change `.env` to point `DATABASE_URL` to your local MySQL server.
- Then, do
  ```
  php bin/console doctrine:database:create
  php bin/console doctrine:migrations:migrate
  php bin/console doctrine:fixtures:load
  php bin/console server:run
  ```
- Check out the game!

## Credentials

Valid player credentials are:

- `beerend` (password also `beerend`)
- `other` (password also `other`)

## Tests

Run `./bin/phpunit` to run all the tests.

## Known limitations

Because I wanted to produce something tangible and usable, but still do this in
a realistic timeframe, I did have to leave out some things.
I'd like to mention them to indicate that I am aware of these limitations and
didn't just gloss over them.

- There isn't a form to create new players.
- A player can lose all their buildings through continued attacks 
  and thus lose any form of resource generation (except through attacking others).