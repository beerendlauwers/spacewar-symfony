<?php

namespace App\Tests\Entity;

use App\Entity\Building;
use App\Entity\BuildingType;
use App\Entity\Player;
use App\Entity\Ship;
use App\Entity\ShipType;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    /**
     * @var BuildingType
     */
    private $buildingType;

    /**
     * @var Building
     */
    private $building;

    /**
     * @var ShipType
     */
    private $shipType;

    /**
     * @var Ship
     */
    private $ship;

    /**
     * @var Player
     */
    private $player;

    protected function setUp()
    {
        $this->buildingType = new BuildingType();
        $this->buildingType
            ->setName("testBuilding")
            ->setMetal(5)
            ->setCrystal(10)
            ->setEnergy(20)
            ->setHealth(100)
            ->setResourceType(1)
            ->setGenerationRate(10);

        $this->building = new Building();
        $this->building->setBuildingType($this->buildingType);

        $this->shipType = new ShipType();
        $this->shipType
            ->setName("testShip")
            ->setMetal(5)
            ->setCrystal(10)
            ->setEnergy(20)
            ->setHealth(100)
            ->setStrength(10);

        $this->ship = new Ship();
        $this->ship->setShipType($this->shipType);

        $this->player = new Player();
        $this->player
            ->setName("player")
            ->setMetal(10)
            ->setCrystal(20)
            ->setEnergy(30);
    }

    protected function tearDown()
    {
        $this->buildingType = null;
        $this->building = null;
        $this->shipType = null;
        $this->ship = null;
        $this->player = null;
    }

    /**
     * @covers Player::purchaseBuilding
     */
    public function testPurchaseSingleBuilding()
    {
        $success = $this->player->purchaseBuilding($this->buildingType);
        $this->assertTrue($success);

        $this->building->setAmount(1)->setPlayer($this->player);

        $this->assertEquals([$this->building], $this->player->getBuildings()->toArray());

        $this->assertEquals(5, $this->player->getMetal());
        $this->assertEquals(10, $this->player->getCrystal());
        $this->assertEquals(10, $this->player->getEnergy());
    }

    /**
     * @covers Player::purchaseBuilding
     */
    public function testPurchaseTwoBuildings()
    {
        $success = $this->player->purchaseBuilding($this->buildingType);
        $this->assertTrue($success);

        $success = $this->player->purchaseBuilding($this->buildingType);
        $this->assertFalse($success);

        $this->building->setAmount(1)->setPlayer($this->player);

        $this->assertEquals([$this->building], $this->player->getBuildings()->toArray());

        $this->assertEquals(5, $this->player->getMetal());
        $this->assertEquals(10, $this->player->getCrystal());
        $this->assertEquals(10, $this->player->getEnergy());
    }

    /**
     * @covers Player::purchaseShip
     */
    public function testPurchaseSingleShip()
    {
        $success = $this->player->purchaseShip($this->shipType);
        $this->assertTrue($success);

        $this->ship->setAmount(1)->setPlayer($this->player);

        $this->assertEquals([$this->ship], $this->player->getShips()->toArray());

        $this->assertEquals(5, $this->player->getMetal());
        $this->assertEquals(10, $this->player->getCrystal());
        $this->assertEquals(10, $this->player->getEnergy());
    }

    /**
     * @covers Player::purchaseShip
     */
    public function testPurchaseTwoShips()
    {
        $success = $this->player->purchaseShip($this->shipType);
        $this->assertTrue($success);

        $success = $this->player->purchaseShip($this->shipType);
        $this->assertFalse($success);

        $this->ship->setAmount(1)->setPlayer($this->player);

        $this->assertEquals([$this->ship], $this->player->getShips()->toArray());

        $this->assertEquals(5, $this->player->getMetal());
        $this->assertEquals(10, $this->player->getCrystal());
        $this->assertEquals(10, $this->player->getEnergy());
    }

    /**
     * Tests that a zero-second difference between times doesn't generate resources.
     *
     * @covers Player::calculateResources
     */
    public function testCalculateResourcesNoOp()
    {
        $now = new \DateTime("now", new \DateTimeZone('Europe/Amsterdam'));
        $this->player->setLastLoginTime($now);

        $previousMetal = $this->player->getMetal();
        $previousCrystal = $this->player->getCrystal();
        $previousEnergy = $this->player->getEnergy();

        $this->player->calculateResources($now);

        $this->assertEquals($previousMetal, $this->player->getMetal());
        $this->assertEquals($previousCrystal, $this->player->getCrystal());
        $this->assertEquals($previousEnergy, $this->player->getEnergy());
    }

    /**
     * Tests that a one-second difference is sufficient to generate resources given
     * enough resource generation buildings.
     *
     * @covers Player::calculateResources
     */
    public function testCalculateResourcesOneSecondAndManyMines()
    {
        // Give the player sufficient resources to purchase 60 metal mines.
        $this->player->setMetal(5 * 60)->setCrystal(10 * 60)->setEnergy(20 * 60);
        $success = $this->player->purchaseBuilding($this->buildingType, 60);

        $this->assertTrue($success);

        $now = new \DateTime("now", new \DateTimeZone('Europe/Amsterdam'));
        $this->player->setLastLoginTime($now);

        $previousMetal = $this->player->getMetal();
        $previousCrystal = $this->player->getCrystal();
        $previousEnergy = $this->player->getEnergy();

        $nowPlusOneSecond = clone $now;
        $nowPlusOneSecond->modify("+1 second");

        $this->player->calculateResources($nowPlusOneSecond);

        $this->assertEquals($previousMetal + $this->buildingType->getGenerationRate(), $this->player->getMetal());
        $this->assertEquals($previousCrystal, $this->player->getCrystal());
        $this->assertEquals($previousEnergy, $this->player->getEnergy());
    }

    /**
     * Tests that a one-second difference is sufficient to generate resources given
     * a high enough resource generation rate.
     *
     * @covers Player::calculateResources
     */
    public function testCalculateResourcesOneSecondAndHighGenerationRate()
    {
        // Modify the building type to have a high generation rate.
        $this->buildingType->setGenerationRate(60);

        $success = $this->player->purchaseBuilding($this->buildingType);
        $this->assertTrue($success);

        $now = new \DateTime("now", new \DateTimeZone('Europe/Amsterdam'));
        $this->player->setLastLoginTime($now);

        $previousMetal = $this->player->getMetal();
        $previousCrystal = $this->player->getCrystal();
        $previousEnergy = $this->player->getEnergy();

        $nowPlusOneSecond = clone $now;
        $nowPlusOneSecond->modify("+1 second");

        $this->player->calculateResources($nowPlusOneSecond);

        $this->assertEquals($previousMetal + 1, $this->player->getMetal());
        $this->assertEquals($previousCrystal, $this->player->getCrystal());
        $this->assertEquals($previousEnergy, $this->player->getEnergy());
    }
}