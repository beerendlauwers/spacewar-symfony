<?php

namespace App\Tests\Entity;

use App\Entity\Battle;
use App\Entity\Building;
use App\Entity\BuildingType;
use App\Entity\Player;
use App\Entity\Ship;
use App\Entity\ShipType;
use PHPUnit\Framework\TestCase;

class BattleTest extends TestCase
{
    /**
     * @var Building
     */
    private $building;

    /**
     * @var Ship
     */
    private $ship;

    /**
     * @var Player
     */
    private $attacker;

    /**
     * @var Player
     */
    private $defender;

    /**
     * @var Battle
     */
    private $battle;

    protected function setUp()
    {
        $buildingType = new BuildingType();
        $buildingType
            ->setName("testBuilding")
            ->setMetal(5)
            ->setCrystal(10)
            ->setEnergy(20)
            ->setHealth(50)
            ->setResourceType(1)
            ->setGenerationRate(10);

        $this->building = new Building();
        $this->building->setBuildingType($buildingType);

        $shipType = new ShipType();
        $shipType
            ->setName("testShip")
            ->setMetal(5)
            ->setCrystal(10)
            ->setEnergy(20)
            ->setHealth(50)
            ->setStrength(10);

        $this->ship = new Ship();
        $this->ship->setShipType($shipType);

        $attacker = new Player();
        $this->attacker = $attacker->setName("attacker")
            ->setPassword("unimportant")
            ->setMetal(0)
            ->setCrystal(0)
            ->setEnergy(0);

        $this->defender = new Player();
        $this->defender->setName("defender")
            ->setPassword("unimportant")
            ->setMetal(1000)
            ->setCrystal(1000)
            ->setEnergy(1000);

        $this->battle = new Battle();
    }

    protected function tearDown()
    {
        $this->attacker = null;
        $this->defender = null;
    }

    /**
     * @covers Battle::doBattle
     */
    public function testBattleWithNoShipsOrBuildings()
    {
        $this->battle->doBattle($this->attacker, $this->defender);

        $this->assertEquals(0, $this->battle->getDamageDone());

        $this->assertGreaterThanOrEqual(0, $this->battle->getMetal());
        $this->assertGreaterThanOrEqual(0, $this->battle->getCrystal());
        $this->assertGreaterThanOrEqual(0, $this->battle->getEnergy());
    }

    /**
     * @covers Battle::doBattle
     */
    public function testBattleWithNoDefendingShips()
    {
        $this->defender->addOrUpdateBuilding($this->building->setAmount(10));
        $this->attacker->addOrUpdateShip($this->ship->setAmount(10));

        $this->battle->doBattle($this->attacker, $this->defender);

        $this->assertGreaterThan(0, $this->battle->getDamageDone());

        $this->assertGreaterThanOrEqual(0, $this->battle->getMetal());
        $this->assertGreaterThanOrEqual(0, $this->battle->getCrystal());
        $this->assertGreaterThanOrEqual(0, $this->battle->getEnergy());

        $this->assertLessThan(10, $this->defender->getBuildingByType($this->building->getBuildingType())->getAmount());
        $this->assertEquals(10, $this->attacker->getShipByType($this->ship->getShipType())->getAmount());
    }

    /**
     * @covers Battle::doBattle
     */
    public function testBattleWithDefendingShipsAndBuildings()
    {
        $this->defender->addOrUpdateBuilding($this->building->setAmount(10));

        $this->ship->setAmount(10);
        $attackingShip = clone $this->ship;

        $this->attacker->addOrUpdateShip($attackingShip);
        $this->defender->addOrUpdateShip($this->ship);

        $this->battle->doBattle($this->attacker, $this->defender);

        $this->assertGreaterThan(0, $this->battle->getDamageDone());

        $this->assertGreaterThanOrEqual(0, $this->battle->getMetal());
        $this->assertGreaterThanOrEqual(0, $this->battle->getCrystal());
        $this->assertGreaterThanOrEqual(0, $this->battle->getEnergy());

        $this->assertEquals(10, $this->defender->getBuildingByType($this->building->getBuildingType())->getAmount());
        $this->assertLessThan(10, $this->defender->getShipByType($this->ship->getShipType())->getAmount());

        $this->assertLessThan(10, $this->attacker->getShipByType($this->ship->getShipType())->getAmount());
    }

    /**
     * @covers Battle::doBattle
     */
    public function testBattleWithBrokeDefender()
    {
        $this->defender->setMetal(0)->setCrystal(0)->setEnergy(0);
        $this->battle->doBattle($this->attacker, $this->defender);

        $this->assertEquals(0, $this->battle->getDamageDone());

        $this->assertEquals(0, $this->battle->getMetal());
        $this->assertEquals(0, $this->battle->getCrystal());
        $this->assertEquals(0, $this->battle->getEnergy());
    }
}