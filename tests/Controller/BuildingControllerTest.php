<?php

namespace App\Tests\Controller;

use App\DataFixtures\BuildingFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BuildingControllerTest extends WebTestCase
{
    public function testAddNewBuilding()
    {
        $this->loadFixtures([
            BuildingFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $client->request('GET', '/player/beerend/building/add/1');

        $this->assertStatusCode(200, $client);

        $response = $client->getResponse();
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $expectedResponseData = ['amount' => 3, 'type' => 'building', 'id' => '1'];
        $this->assertEquals($expectedResponseData, $responseData);
    }

    public function testAddNewBuildingOtherPlayer()
    {
        $this->loadFixtures([
            BuildingFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);
        $client->catchExceptions(false);

        $this->expectException(AccessDeniedException::class);

        $client->request('GET', '/player/other/building/add/1');

        $this->assertStatusCode(403, $client);
    }
}