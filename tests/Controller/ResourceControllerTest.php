<?php

namespace App\Tests\Controller;

use App\DataFixtures\PlayerFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ResourceControllerTest extends WebTestCase
{
    public function testGetResources()
    {
        $this->loadFixtures([
            PlayerFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $client->request('GET', '/player/beerend/resources');

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $expectedResponseData = ['metal' => 100, 'crystal' => 100, 'energy' => 100];
        $this->assertEquals($expectedResponseData, $responseData);
    }

    public function testGetOtherPlayerResources()
    {
        $this->loadFixtures([
            PlayerFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);
        $client->catchExceptions(false);

        $this->expectException(AccessDeniedException::class);

        $client->request('GET', '/player/other/resources');

        $this->assertStatusCode(403, $client);
    }
}