<?php

namespace App\Tests\Controller;

use App\DataFixtures\BuildingFixtures;
use App\DataFixtures\ShipFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class GameControllerTest extends WebTestCase
{
    public function testGameLoginCorrectCredentialsRedirectsToPlayerOverview()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertStatusCode(200, $client);

        $form = $crawler->selectButton('login')->form();

        $form['playerName'] = 'beerend';
        $form['password'] = 'beerend';

        $client->submit($form);

        $this->assertStatusCode(302, $client);

        $client->followRedirect();

        $this->assertRegExp('/\/player\/beerend/', $client->getRequest()->getUri());
    }

    public function testGameLoginInvalidCredentials()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertStatusCode(200, $client);

        $form = $crawler->selectButton('login')->form();

        $form['playerName'] = 'beerend';
        $form['password'] = 'invalid';

        $client->submit($form);

        $this->assertStatusCode(302, $client);

        $crawler = $client->followRedirect();

        $this->assertTrue($crawler->filter('html:contains("Invalid credentials.")')->count() > 0);
    }

    public function testLogoutRedirectsToLoginPage()
    {
        $this->loadFixtures([
            BuildingFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $client->request('GET', '/player/beerend');

        $this->assertStatusCode(200, $client);

        $client->request('GET', '/logout');

        $this->assertStatusCode(302, $client);

        $crawler = $client->followRedirect();

        $this->assertTrue($crawler->filter('html:contains("Game Login")')->count() > 0);
    }

    public function testAnonymousVisitOnPlayerOverviewRedirectsToLogin()
    {
        $client = static::createClient();

        $client->request('GET', '/player/beerend');

        $this->assertStatusCode(302, $client);

        $crawler = $client->followRedirect();

        $this->assertStatusCode(200, $client);

        $this->assertTrue($crawler->filter('html:contains("Game Login")')->count() > 0);
    }

    public function testResourcesAreShownOnPlayerOverview()
    {
        $this->loadFixtures([
            BuildingFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $crawler = $client->request('GET', '/player/beerend');

        $this->assertStatusCode(200, $client);

        $metalCounter = $crawler->filter('#player-metal');
        $crystalCounter = $crawler->filter('#player-crystal');
        $energyCounter = $crawler->filter('#player-energy');
        $this->assertEquals(100, $metalCounter->text());
        $this->assertEquals(100, $crystalCounter->text());
        $this->assertEquals(100, $energyCounter->text());
    }

    public function testBuildingsAreShownOnPlayerOverview()
    {
        $this->loadFixtures([
            BuildingFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $crawler = $client->request('GET', '/player/beerend');

        $this->assertStatusCode(200, $client);

        $table = $crawler->filter('.game-buildings');
        $this->assertCount(3, $table->filter('tbody tr'));
    }

    public function testShipsAreShownOnPlayerOverview()
    {
        $this->loadFixtures([
            ShipFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $crawler = $client->request('GET', '/player/beerend');

        $this->assertStatusCode(200, $client);

        $table = $crawler->filter('.game-ships');
        $this->assertCount(3, $table->filter('tbody tr'));
    }

    public function testConstructingABuildingIncreasesCountAndDecreasesResources()
    {
        $this->loadFixtures([
            BuildingFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $client->request('GET', '/player/beerend/building/add/1');

        $this->assertStatusCode(200, $client);

        $crawler = $client->request('GET', '/player/beerend');

        $metalCounter = $crawler->filter('#player-metal');
        $crystalCounter = $crawler->filter('#player-crystal');
        $energyCounter = $crawler->filter('#player-energy');
        $this->assertEquals(50, $metalCounter->text());
        $this->assertEquals(80, $crystalCounter->text());
        $this->assertEquals(70, $energyCounter->text());

        $this->assertEquals(3, $crawler->filter('#building-type-1 .amount')->text());
    }

    public function testConstructingAShipIncreasesCountAndDecreasesResources()
    {
        $this->loadFixtures([
            ShipFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $client->request('GET', '/player/beerend/ship/add/1');

        $this->assertStatusCode(200, $client);

        $crawler = $client->request('GET', '/player/beerend');

        $metalCounter = $crawler->filter('#player-metal');
        $crystalCounter = $crawler->filter('#player-crystal');
        $energyCounter = $crawler->filter('#player-energy');
        $this->assertEquals(50, $metalCounter->text());
        $this->assertEquals(80, $crystalCounter->text());
        $this->assertEquals(70, $energyCounter->text());

        $this->assertEquals(6, $crawler->filter('#ship-type-1 .amount')->text());
    }

    public function testLaunchingABattleAddsBattleRow()
    {
        $this->loadFixtures([
            ShipFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $crawler = $client->request('GET', '/player/beerend');

        $form = $crawler
            ->filter('form[name=attack]')
            ->form(['attack[otherPlayer]' => 1]);

        $crawler = $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertCount(1, $crawler->filter('.game-battles tbody tr'));
    }

    public function testGettingAttackedAddsBattleRow()
    {
        $this->loadFixtures([
            ShipFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'other',
            'PHP_AUTH_PW'   => 'other',
        ]);

        $crawler = $client->request('GET', '/player/other');

        $form = $crawler
            ->filter('form[name=attack]')
            ->form(['attack[otherPlayer]' => 0]);

        $client->submit($form);

        $this->assertStatusCode(200, $client);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $crawler = $client->request('GET', '/player/beerend');

        $this->assertStatusCode(200, $client);

        $this->assertCount(1, $crawler->filter('.game-battles tbody tr'));
    }
}