<?php

namespace App\Tests\Controller;

use App\DataFixtures\ShipFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ShipControllerTest extends WebTestCase
{
    public function testAddNewShip()
    {
        $this->loadFixtures([
            ShipFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);

        $client->request('GET', '/player/beerend/ship/add/1');

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getContent());
        $responseData = json_decode($response->getContent(), true);

        $expectedResponseData = ['amount' => 6, 'type' => 'ship', 'id' => '1'];
        $this->assertEquals($expectedResponseData, $responseData);
    }

    public function testAddNewShipOtherPlayer()
    {
        $this->loadFixtures([
            ShipFixtures::class,
        ]);

        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'beerend',
            'PHP_AUTH_PW'   => 'beerend',
        ]);
        $client->catchExceptions(false);

        $this->expectException(AccessDeniedException::class);

        $client->request('GET', '/player/other/ship/add/1');

        $this->assertStatusCode(403, $client);
    }
}