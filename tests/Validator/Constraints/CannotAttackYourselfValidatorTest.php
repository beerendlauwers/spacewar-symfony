<?php

namespace App\Tests\Validator\Constraints;

use App\Entity\Player;
use App\Validator\Constraints\CannotAttackYourself;
use App\Validator\Constraints\CannotAttackYourselfValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers CannotAttackYourselfValidator::
 */
class CannotAttackYourselfValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator()
    {
        return new CannotAttackYourselfValidator();
    }

    /**
     * @covers CannotAttackYourselfValidator::validate
     */
    public function testSamePlayerIsInvalid()
    {
        $player = new Player();
        $player->setName("username")->setLastLoginTime(new \DateTime(('now')));

        $constraint = new CannotAttackYourself(['player' => $player]);
        $this->validator->validate($player, $constraint);

        $this->buildViolation($constraint->message)->assertRaised();
    }

    /**
     * @covers CannotAttackYourselfValidator::validate
     */
    public function testDifferentPlayersAreValid()
    {
        $player = new Player();
        $player->setName("username")->setLastLoginTime(new \DateTime(('now')));

        $otherPlayer = new Player();
        $otherPlayer->setName("other")->setLastLoginTime(new \DateTime(('now')));

        $constraint = new CannotAttackYourself(['player' => $player]);
        $this->validator->validate($otherPlayer, $constraint);

        $this->assertNoViolation();
    }
}