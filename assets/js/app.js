/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');

$(document).ready(function () {
    setInterval(pollResources, 3000);

    $('.build-item').on('click', function (e) {
        e.preventDefault();
        var link = $(e.currentTarget);

        $.ajax({
            method: 'POST',
            url: link.attr('href')
        }).done(function (data) {
            if (!data.amount) {
                return;
            }

            var itemRow = $('#' + data.type + '-type-' + data.id);
            itemRow.find('.amount').text(data.amount);
            pollResources();
        }).fail(function (r) {
            if (r.responseJSON.error) {
                alert(r.responseJSON.error);
            }
        })

    });
});

function pollResources() {
    var url = $('#resources').attr("data-route");

    $.ajax({
        method: 'GET',
        url: url
    }).done(function (data) {
        $('#player-metal').text(data.metal);
        $('#player-crystal').text(data.crystal);
        $('#player-energy').text(data.energy);
    })
}